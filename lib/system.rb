require 'rubygems'
require 'json'
require 'pp'

require './lib/mass.rb'
require './lib/system_gen.rb'

class System
	def initialize
		@prev_time = Time.now.to_i
		refresh()
	end

	def update selection_x, selection_y
		if @lock == true then return end
		$selected_mass = nil
		@masses.each do |mass|
			mass.selected?(selection_x, selection_y)
		end

		curr_time = Time.now.to_i
		delta = curr_time - @prev_time
		if delta >= $orbit_time
			@masses.each { |mass| mass.move(nil, nil) }
			@prev_time = curr_time
		end
	end

	def draw
		if @lock == true then return end
		Gosu.translate($offset_x, $offset_y) do
			interval = 25 * $scale

			line_offset_x = (($offset_x - ($width / 2)) / (25 * $scale)).floor
			puts "line_offset_x = #{line_offset_x}"
			line_offset_y = (($offset_y - ($width / 2)) / (25 * $scale)).floor
			puts "line_offset_y = #{line_offset_y}"
			
			# vertical lines
			((-10 - line_offset_x)..(10 - line_offset_x)).each do |lx|
				Gosu::draw_line(lx * interval,
								-$offset_y + 0,
								Colors::UI_WHITE,
								lx * interval,
								-$offset_y + $height,
								Colors::UI_WHITE)
			end

			# horizontal lines
			((-10 - line_offset_y)..(10 - line_offset_y)).each do |ly|
				Gosu::draw_line(-$offset_x + 0,
								ly * interval,
								Colors::UI_WHITE,
								-$offset_x + $width,
								ly * interval,
								Colors::UI_WHITE)
			end

			@masses.each do |mass|
				mass.draw
			end
		end
	end

	def refresh
		@lock = true

		@masses = SystemGen.generate_system(0)
		#@masses = []

		#files = Dir['res/masses/*.json']

		#files.reject! { |file| file.include? "other" }

		#files.each do |file|
			#json = File.read(file)
			#mass = JSON.parse(json)

			#type = case mass["type"]
			#when "STAR"
				#MassType::STAR
			#when "PLANET"
				#MassType::PLANET
			#when "MOON"
				#MassType::MOON
			#when "ASTEROID"
				#MassType::ASTEROID
			#when "STATION"
				#MassType::STATION
			#else
				#MassType::STATION
			#end

			#@masses << Mass.new(mass["name"], mass["x"], mass["y"], type, mass["log"], mass["links"])
		#end
		@lock = false
	end

	def save name
		string =  "[" + @masses.map { |mass| mass.serialize }.join(",") + "]"
		File.open("res/#{name}.json", 'w') { |file| file.write(string) }
	end

	def load name
		@lock = true
		def convert_to_type string
			case string
			when "STAR"
				MassType::STAR
			when "PLANET"
				MassType::PLANET
			when "MOON"
				MassType::MOON
			when "ASTEROID"
				MassType::ASTEROID
			when "STATION"
				MassType::STATION
			else
				MassType::STATION
			end
		end

		def convert_to_obj obj
			type = convert_to_type(obj["type"])
			children = obj["children"].map { |child| convert_to_obj(child) }
			return Mass.new(obj["name"], obj["x"], obj["y"], obj["radius"], type, obj["logs"], obj["links"], obj["coords"], children)
		end

		json = JSON.parse(File.read("res/#{name}.json"))
		mass_list = json.map { |obj| convert_to_obj(obj) }

		@masses = mass_list
		@lock = false
	end
end
