require 'gosu'

require './lib/galaxy_overview_loop.rb'
require './lib/planet_info_loop.rb'

class GameWindow < Gosu::Window
	def initialize width = $width, height = $height, fullscreen = false
		super width, height
		self.caption = "round the planet"

		@galaxy_overview_loop = GalaxyOverview.new
		@menu = nil
		@planet_info_loop = nil
		$state = State::GALAXY_VIEW
	end

	def update
		case $state
		when State::MENU
			@menu.update(mouse_x, mouse_y)
		when State::GALAXY_VIEW
			@galaxy_overview_loop.update(mouse_x, mouse_y)
		when State::PLANET_INFO
			@planet_info_loop.update if @planet_info_loop != nil
		else
		end
	end

	def draw
		case $state
		when State::MENU
			@menu.draw
		when State::GALAXY_VIEW
			@galaxy_overview_loop.draw
		when State::PLANET_INFO
			@planet_info_loop.draw if @planet_info_loop != nil
		else
		end
	end

	def button_down id
		case id
		when Gosu::KB_ESCAPE
			self.close
		end

		case $state
		when State::MENU
			@menu.button_down(id)
		when State::GALAXY_VIEW
			@galaxy_overview_loop.button_down(id)
		when State::PLANET_INFO
			@planet_info_loop.button_down(id) if @planet_info_loop != nil
		end
	end

	def button_up id
		case $state
		when State::MENU
			@menu.button_up(id)
		when State::GALAXY_VIEW
			case id
			when Gosu::MS_RIGHT
				if $selected_mass != nil
					@planet_info_loop = PlanetInfo.new($selected_mass)
					$state = State::PLANET_INFO
				end
			end
			@galaxy_overview_loop.button_up(id)
		when State::PLANET_INFO
			case id
			when Gosu::KB_Q
				$state = State::GALAXY_VIEW
				@planet_info_loop = nil
			end
			@planet_info_loop.button_up(id) if @planet_info_loop != nil
		end
	end

	def needs_cursor?
		true
	end
end

module Colors
	UI_WHITE = Gosu::Color.rgba(255, 255, 255, 50)
	UI_YELLOW = Gosu::Color.rgba(0, 255, 255, 50)
end

module State
	MENU = 0
	GALAXY_VIEW = 1
	PLANET_INFO = 2
end
