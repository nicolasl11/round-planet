require 'gosu'

require './lib/system.rb'

class GalaxyOverview
	def initialize
		@info_font = Gosu::Font.new(14)
		@scan_line_font = Gosu::Font.new(10)

		@scroll_pressed = false
		@prev_mouse_x = 0
		@prev_mouse_y = 0

		@system = System.new
	end

	def update mouse_x, mouse_y
		pan(mouse_x, mouse_y) if @scroll_pressed
		@prev_mouse_x, @prev_mouse_y = mouse_x, mouse_y
		@system.update((mouse_x - $offset_x) / $scale, (mouse_y - $offset_y) / $scale)
		@info_text = "Sector: #{(((mouse_x - $offset_x) / $scale) / 25).floor}, #{(((mouse_y - $offset_y) / $scale) / 25).floor}"
	end

	def draw
		selection_text = if $selected_mass != nil
							"#{$selected_mass.type_name.capitalize}: #{$selected_mass.name} (#{$selected_mass.x},#{$selected_mass.y})"
						else
							""
						end

		 #sector text
		@info_font.draw(@info_text, 10, 10, 1, 1.0, 1.0, Gosu::Color::YELLOW)

		@info_font.draw(selection_text, 10, $height - 20, 1, 1.0, 1.0, Gosu::Color::YELLOW)

		#draw_help

		@system.draw
	end

	def button_down id
		case id
		when Gosu::MS_LEFT
			@scroll_pressed = true
		end
	end

	def button_up id 
		case id
		when Gosu::KB_D
			$offset_x += 100 / $scale
		when Gosu::KB_A
			$offset_x -= 100 / $scale
		when Gosu::KB_S
			$offset_y += 100 / $scale
		when Gosu::KB_W
			$offset_y -= 100 / $scale
		when Gosu::KB_C
			self.refresh
		when Gosu::KB_R
			$offset_x = $width / 2
			$offset_y = $height / 2
		when Gosu::MS_LEFT
			@scroll_pressed = false
		when Gosu::MS_WHEEL_DOWN
			$scale = case
						when $scale - 1 < 2 then 2
						else $scale - 1
						end
			$offset_x -= $offset_x % 100
			$offset_y -= $offset_y % 100
		when Gosu::MS_WHEEL_UP
			$scale = case
						when $scale + 1 > 8 then 8
						else $scale + 1
						end
			$offset_x -= $offset_x % 100
			$offset_y -= $offset_y % 100
		when Gosu::KB_G
			self.save("main")
		when Gosu::KB_H
			self.load("main")
		end
	end

	def draw_help
		@info_font.draw("wasd: move", $width - 100, $height - 80, 1, 1.0, 1.0, Gosu::Color::YELLOW)
		@info_font.draw("r: reset (0,0)", $width - 100, $height - 60, 1, 1.0, 1.0, Gosu::Color::YELLOW)
	end

	def refresh
		@system.refresh
	end

	def save name
		@system.save(name)
	end

	def load name
		@system.load(name)
	end

	def pan mouse_x, mouse_y
		#puts "offset: #{$offset_x}, #{$offset_y}"
		$offset_x += (mouse_x - @prev_mouse_x)
		$offset_y += (mouse_y - @prev_mouse_y)
		@prev_mouse_x, @prev_mouse_y = mouse_x, mouse_y
	end
end
