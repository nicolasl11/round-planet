require 'rubygems'
require 'json'
require 'pp'

class Status
	attr_accessor :settings
	attr_reader :file

	@file = "res/savefile.json"

	def initialize
		@settings = {}
	end

	def save
		string = JSON.generate(@settings)

		File.open(@file, 'w') { |file| file.write(string) }
		puts "written #{@file}"
	end

	def load
		json = File.read(@file)

		@settings = JSON.parse(json)
		puts "loaded #{@file}"
	end
end
