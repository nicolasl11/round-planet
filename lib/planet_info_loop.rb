class PlanetInfo
	def initialize mass
		@mass = mass

		@info_font = Gosu::Font.new(14)
		@title_font = Gosu::Font.new(18)
		@font_color = Gosu::Color::YELLOW
		@ui_color = Gosu::Color.rgba(255, 255, 0, 100)
		@ui_thikness = 2
		@ui_offset_x = 2
		@ui_offset_y = 2

		@text_offset_x = 8

		@view_index = View::LOG
		@max_view_index = View::CONNECTIONS
		@log_index = 0

		@log_buffer = seperate_log
	end

	def reset
		@view_index = View::LOG
		@log_index = 0
		@log_buffer = seperate_log
	end

	def update
	end

	def draw
		draw_title

		case @view_index
		when View::LOG
			draw_log
		when View::CONNECTIONS
			draw_connections
		end

		draw_help
	end

	def button_down id
		#case id
		#end
	end

	def button_up id
		case id
		when Gosu::KB_A
			self.scroll_left
		when Gosu::KB_D
			self.scroll_right
		when Gosu::KB_W
			self.previous_view
		when Gosu::KB_S
			self.next_view
		end
	end

	def scroll_left
		if @view_index != View::LOG then return end
		@log_index = case
					  when @log_index - 1 <= 0 then 0
					  else @log_index - 1
					  end
	end

	def scroll_right
		if @view_index != View::LOG then return end
		length = @mass.logs.length
		@log_index = case
					  when @log_index + 1 >= length then length - 1
					  else @log_index + 1
					  end
	end

	def previous_view
		@view_index = case
					  when @view_index - 1 < 0 then @max_view_index
					  else @view_index - 1
					  end
	end

	def next_view
		@view_index = case
					  when @view_index + 1 > @max_view_index then 0
					  else @view_index + 1
					  end
	end

	def draw_help
		if @view_index == View::LOG
			@info_font.draw("ad: pg back/fwd", $width - 130, $height - 70, 1, 1.0, 1.0, @font_color)
		end
		@info_font.draw("ws: previous/next view", $width - 130, $height - 55, 1, 1.0, 1.0, @font_color)
		@info_font.draw("q: back", $width - 130, $height - 40, 1, 1.0, 1.0, @font_color)
	end

	def draw_black_box x, y, width, height, color, thickness
		Gosu::draw_rect(x, y, width, height, color)

		Gosu::draw_rect(thickness, thickness,
			width - thickness * 2,
			height - thickness * 2,
			Gosu::Color::BLACK)
	end

	def draw_title
		if @mass == nil then return end

		mass = @mass

		Gosu.translate(@ui_offset_x, @ui_offset_y) do
			t_width = $width - @ui_offset_x

			draw_black_box(0, 0,
						   t_width - @ui_offset_x, @title_font.height * 2,
						   @ui_color, @ui_thikness)

			@title_font.draw("Info for #{mass.name}", @text_offset_x , 12, 1, 1.0, 1.0, @font_color)
		end
	end

	def draw_log
		if @mass == nil then return end

		Gosu.translate(@ui_offset_x, 40) do
			mass = @mass

			t_width = $width - @ui_offset_x
			t_height = $height - 40

			draw_black_box(0, 0,
						  t_width - @ui_offset_x, t_height - @ui_offset_y,
						  @ui_color, @ui_thikness)

			if mass == nil or mass.logs == [] or mass.logs == nil
				@info_font.draw("No log entries found.", @text_offset_x, 10, 1, 1.0, 1.0, @font_color)
				return
			end

			@title_font.draw("Event log", @text_offset_x, 10, 1, 1.0, 1.0, @font_color)
			@info_font.draw("Page #{@log_index + 1} out of #{mass.logs.length}", @text_offset_x, 10 + @title_font.height * 1.5, 1, 1.0, 1.0, @font_color)

			if @log_buffer == nil or @log_buffer == []
				@log_buffer = seperate_log
			end

			line_counter = 0
			@log_buffer[@log_index].each do |line|
				@info_font.draw(line,
								@text_offset_x, 10 + @title_font.height * 1.5 + @info_font.height * 1.5 + @info_font.height * line_counter,
									1, 1.0, 1.0, @font_color)
				line_counter += 1
			end
		end
	end

	def draw_connections
		if @mass == nil then return end

		Gosu.translate(@ui_offset_x, 40) do
			mass = @mass

			t_width = $width - @ui_offset_x
			t_height = $height - 40

			draw_black_box(0, 0,
						  t_width - @ui_offset_x, t_height - @ui_offset_y,
						  @ui_color, @ui_thikness)

			if mass == nil or mass.links == [] or mass.links == nil
				@info_font.draw("No sublight-connections.", @text_offset_x, 10, 1, 1.0, 1.0, @font_color)
				return
			end

			index = 0

			@title_font.draw("Sublight-connection", @text_offset_x, 10, 1, 1.0, 1.0, @font_color)
			mass.links.each do |link|
				text = "#{link["name"]} #{link["x"]},#{link["y"]} (distance: #{Gosu::distance(mass.x, mass.y, link["x"], link["y"]).round})"
				x = 10
				y = 10 + @title_font.height * 2 + @info_font.height * index

				@info_font.draw(text, x, y, 1, 1.0, 1.0, @font_color)

				index += 1
			end
		end
	end

	def seperate_log
		if @mass == nil then return [] end

		mass = @mass
		master_buffer = []
		mass.logs.each do |log|
			log_buffer = []
			buffer = []
			log.split(/\s/).each do |word|
				buffer << word

				if @info_font.text_width(buffer.join(' ')) > $width - @text_offset_x - 100
					log_buffer << buffer.join(' ')
					buffer.clear
				end
			end

			log_buffer << buffer.join(' ')
			master_buffer << log_buffer
		end

		master_buffer
	end
end

module View
	LOG = 0
	CONNECTIONS = 1
end
