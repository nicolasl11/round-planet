module SystemGen
	def SystemGen.generate_system seed, others = {}
		min_star_dist = others["min_star_dist"] or 300
		max_star_dist = others["max_star_dist"] or 600

		home_star = Mass.new(SystemGen.name_gen(), 0, 0, 0, MassType::STAR, [], [], [{"x" => 0, "y" => 0}])

		planet_count = Gosu.random(2, (others["amount_of_planets"] or 10)).round
		(1..planet_count+1).each do |index|
			radius = Gosu.distance(home_star.x, home_star.y, home_star.x, home_star.y - 200 / planet_count * index).round
			coordinates = []

			0.step(360, 1).each do |a|
				a2 = a - 1
				cx = home_star.x
				cy = home_star.y

				coordinates << {"x" => (cx + Gosu.offset_x(a, radius)).round, "y" => (cy + Gosu.offset_y(a, radius)).round}
			end

			rand = Gosu.random(0, coordinates.length)
			x = coordinates[rand]["x"]
			y = coordinates[rand]["y"]

			planet = Mass.new(SystemGen.name_gen(), x, y, radius, MassType::PLANET, [], [], coordinates)
			moon_count = 0
			moon_count = case Gosu.random(0, 11)
						 when 0..5 then 0
						 when 5..7 then 1
						 when 7..8 then 2
						 when 8..9 then 3
						 when 9..10 then 4
						 else 0
						 end

			moon_index = 0
			moon_count.times do
				moon_radius = Gosu.distance(planet.x, planet.y, planet.x, planet.y - 20 / moon_count * (moon_index + 1)).round
				moon_coordinates = []

				0.step(360, 1).each do |moon_a|
					moon_a2 = moon_a - 1
					moon_cx = planet.x
					moon_cy = planet.y

					moon_coordinates << {"x" => (moon_cx + Gosu.offset_x(moon_a, moon_radius)).round, "y" => (moon_cy + Gosu.offset_y(moon_a, moon_radius)).round}
				end

				moon_rand = Gosu.random(0, moon_coordinates.length)
				moon_x = moon_coordinates[moon_rand]["x"]
				moon_y = moon_coordinates[moon_rand]["y"]

				planet.add_child(Mass.new(SystemGen.name_gen(), moon_x, moon_y, moon_radius, MassType::MOON, [], [], moon_coordinates))
				moon_index += 1
			end

			home_star.add_child(planet)
		end

		return [home_star]
	end

	def SystemGen.name_gen
		(0...10).map { (65 + rand(26)).chr }.join
	end
end
