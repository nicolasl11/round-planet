class Mass
	attr_reader :x
	attr_reader :y
	attr_reader :name
	attr_reader :type
	attr_reader :logs
	attr_reader :links
	attr_reader :children
	attr_reader :coords
	attr_reader :radius
	attr_reader :coord_index

	def initialize name, x, y, radius, type = MassType::STATION, logs = [], links = [], coords = [], children = []
		@name = name
		@x = x
		@y = y
		@type = type
		@logs = logs
		@coords = coords
		@radius = radius
		@links = links # {name => name, x => x, y => y}
		@children = children
		@coord_index = @coords.index { |coord| coord["x"] == @x and coord["y"] == @y}
	end

	def update
	end

	def draw
		@children.each { |child| child.draw }

		rx = -$offset_x
		ry = -$offset_y

		sx = @x * $scale
		sy = @y * $scale

		if $selected_mass != nil and @name == $selected_mass.name
			Gosu::draw_rect(sx - 4, sy - 4, $scale + 8, $scale + 8, Colors::UI_WHITE)
			Gosu::draw_rect(sx - 2, sy - 2, $scale + 4, $scale + 4, Gosu::Color::BLACK)

			Gosu::draw_line(sx, sy + $scale + 4, Colors::UI_WHITE, rx + 40, ry + $height - 40, Colors::UI_WHITE)

			#if @links != nil
				#@links.each do |link|
					#Gosu::draw_line(draw_x, draw_y, Gosu::Color::BLUE, link["x"] * $scale - $offset_x * $scale, link["y"] * $scale - $offset_y * $scale, Gosu::Color::BLUE)
				#end
			#end

			@coords.each_index do |index|
				x1 = @coords[index]["x"]
				y1 = @coords[index]["y"]

				x2 = @coords[0]["x"]
				y2 = @coords[0]["y"]

				if @coords[index + 1] != nil
					x2 = @coords[index + 1]["x"]
					y2 = @coords[index + 1]["y"]
				end

				Gosu.draw_line(x1 * $scale, y1 * $scale, Colors::UI_WHITE, x2 * $scale, y2 * $scale, Colors::UI_WHITE)
			end

			@children.each do |child|
				Gosu.draw_line(@x * $scale, @y * $scale, Gosu::Color::WHITE, child.x * $scale, child.y * $scale, Gosu::Color::WHITE)
			end
		end

		Gosu::draw_rect(@x * $scale, @y * $scale, $scale, $scale, @type)
	end
	
	def type_name
		case @type
		when MassType::MOON
			"MOON"
		when MassType::STAR
			"STAR"
		when MassType::STATION
			"STATION"
		when MassType::ASTEROID
			"ASTEROID"
		when MassType::PLANET
			"PLANET"
		else
			"STATION"
		end
	end

	def add_log entry
		if entry == nil then return end
		@logs << entry
	end

	def add_child child
		if child == nil then return end
		@children << child
	end

	def move px, py
		if @radius != 0
			@coords = gen_orbit(px, py, @radius)
			@coord_index = case
						   when @coord_index + 1 >= @coords.length then 0
						   else @coord_index + 1
						   end

			@x = @coords[@coord_index]["x"]
			@y = @coords[@coord_index]["y"]
		end

		@children.each { |child| child.move(@x, @y) }
	end

	def gen_orbit cx, cy, radius
		coordinates = []
		0.step(360, 1).each do |a|
			a2 = a - 1

			coordinates << {"x" => (cx + Gosu.offset_x(a, radius)).round, "y" => (cy + Gosu.offset_y(a, radius)).round}
		end

		coordinates
	end

	def selected? selection_x, selection_y
		if selection_x - 4 < @x and selection_x + 4 + $scale > @x + $scale and selection_y - 4 < @y and selection_y + 4 + $scale > @y + $scale
			$selected_mass = self
		else
			@children.each do |mass|
				mass.selected?(selection_x, selection_y)
			end
		end
	end

	def serialize
		children = @children.map { |child| child.serialize }
		json_children = "[" + children.join(',') + "]"
		obj = {
			"name" => @name,
			"x" => @x,
			"y" => @y,
			"type" => type_name(),
			"logs" => @logs,
			"coords" => @coords,
			"coord_index" => @coord_index,
			"radius" => @radius,
			"links" => @links,
			"children" => "__vermeerse__thing__sempatomary__"
		}

		string = JSON.generate(obj)
		string = string.gsub(/\"__vermeerse__thing__sempatomary__\"/, json_children)
		return string
	end
end

module MassType
	STATION = Gosu::Color::WHITE
	PLANET = Gosu::Color.rgba(188, 121, 62, 255) # crisp brown
	STAR = Gosu::Color::YELLOW
	ASTEROID = Gosu::Color.rgba(168, 168, 168, 255) # light gray
	MOON = Gosu::Color.rgba(45, 88, 229, 255) # smooth blue
end
