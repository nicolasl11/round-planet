require './lib/game_window.rb'

$width = 640
$height = 480

$scale = 4

$offset_x = $width / 2
$offset_y = $height / 2

$selected_mass = nil

$orbit_time = 5

class Game
	def initialize
		@window = GameWindow.new
	end

	def start
		@window.show
	end
end
